import React, { Component } from 'react';
import './App.css';
import { runErb } from './ErbParser';

class App extends Component {
  constructor() {
    super();
    this.state = {
      showSource: true,
      displayText: [],
      historyText: [],
      source: '',
      commandInputValue: '',
    };

    this.passClickOrKeyPressEventToErb = event => {
      this.commandInput.focus();
      if (this.onEnterPressListener) {
        if (event && event.key === 'Enter' && this.onEnterPressListener(this.state.commandInputValue)) {
          // Listener used this and now wants us to clear the text box
          this.setState({commandInputValue: ''});
        }
      } else if(this.onClickOrKeyPressListener) {
        if (this.onClickOrKeyPressListener(event ? event.key : null)) {
          // Listener used this and now wants us to clear the text box
          this.setState({commandInputValue: ''});
        }
      }
    };
  }
  onLinkClicked(linkClickedValue) {
    if (this.onEnterPressListener)
      this.onEnterPressListener(linkClickedValue);
    if (this.onClickOrKeyPressListener)
      this.onClickOrKeyPressListener(linkClickedValue);
  }

  componentDidMount() {
    this.commandInput.focus();
    runErb(changeState => this.setState(changeState),
        onEnterPressListener => this.onEnterPressListener = onEnterPressListener,
        onClickOrKeyPressListener => this.onClickOrKeyPressListener = onClickOrKeyPressListener,
        linkClickedValue => this.onLinkClicked(linkClickedValue));
  }

  componentDidUpdate() {
    this.commandInput.scrollIntoView(true);
  }

  render() {
    let i = 0;
    const lineNumbers = this.state.source && <pre>{this.state.source.split('\n').map(() =>
        (++i === this.state.errorLineNumber) ? (i + ' -->\n') : (i + '\n')
    )}</pre>;
    return (
      <div className="App"  onClick={this.passClickOrKeyPressEventToErb}>
        {/*<header className="header">
          <h1 className="title">JS ErbRunner!</h1>
    </header>*/}
        <div className="outputParentDiv">
          <div className="historyDiv">
            {this.state.historyText}
          </div>
          <div className="outputDiv">
            {this.state.displayText}
          </div>
          <input ref={x => this.commandInput = x} value={this.state.commandInputValue} type="text" className="commandInput" onKeyDown={this.passClickOrKeyPressEventToErb} onChange={e => this.onEnterPressListener && this.setState({commandInputValue: e.target.value}) }/>
        </div>
        { this.state.errorMsg && /* Only show source and error if there's an actual error */
          <pre className="errorMsg">
            {this.state.errorMsg}
          </pre>
        }
        { (this.state.errorMsg || this.state.showSource) &&
          <div className="sourceDivs">
            <pre className="sourceDiv">
              {this.state.source}
            </pre>
            <pre className="sourceDiv">
              {lineNumbers}
            </pre>
            <pre className="sourceDiv">
              {this.state.sourceJS}
            </pre>
          </div>
        }
      </div>
    );
  }
}

export default App;
