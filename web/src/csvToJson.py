#!/usr/bin/python3

from pathlib import Path
import re
import json
import csv
import sys

res = {}
commentRegex = re.compile(r'[;；]')

def arrayInsert(arr, index, value):
    if len(arr) <= index:
        arr.extend((index + 1 - len(arr)) * [None])
    arr[index] = value

for path in Path('../../CSV').glob('*.csv'):
    fullfilename = str(path)

    name = path.name[:-4].upper()
    idToName = {}
    nameToId = {}
    if name == 'VARIABLESIZE':
        res[name] = idToName
    else:
        res[name + 'ID'] = nameToId
        res[name + 'NAME'] = idToName
    try:
        with open(fullfilename, encoding='cp932') as csvfile:
            for row in csvfile:
                row = row.strip()
                if row != '' and not (row.startswith(';') or row.startswith('；')):
                    row = commentRegex.split(row,1)[0].split(',')
                    if len(row) >= 2:
                        if name == 'VARIABLESIZE':
                            idToName[row[0]] = int(row[1])
                        else:
                            idToName[row[0]] = row[1]
                        # Only add to nameToId if it is actually a number
                        try:
                            nameToId[row[1]] = int(row[0])
                        except:
                            pass
    except Exception as e:
        print("Can't read", fullfilename, e, file=sys.stderr)
    if len(nameToId) == 0 and name != 'VARIABLESIZE':
        del res[name + 'ID']

for key, translate in res.items():
    if key.endswith("_TRNAME") and len(translate) > 0 and key != 'ALL_TRNAME':
        original = res[key[:-7] + 'NAME'] #e.g. ITEMNAME
        for key2, originalString in original.items():
            if originalString in translate:
                original[key2] = translate[originalString] # Replace with translation. e..g

subkeyToVarname = {
    "能力": "ABL",
    "番号": "ID",
    "基礎": "BASE",
    "フラグ": "CFLAG",

    "EQUIP": "EQUIP",
    "CSTR": "CSTR",
    "名前": "CALLNAME", #This is a bit different from the others since this is the name of the character
    "呼び名": "CVSCALLNAME",
    "経験": "EXP",
    "素質": "TALENT",
}

pathlist = Path('../../CSV').glob('*/**/*.csv')
for path in pathlist:
    fullfilename = str(path)
    name = path.name
    match = re.match(r'([^0-9]+)([0-9]+)[_ ;.]?(.*).csv', name)
    if match:
        group, number, name = match.groups()
        number = int(number)
        group = group.upper() # e.g. 'CHARA' for all the characters
        #if not group in res:
        #    res[group] = {}
        #arr = res[group]
        #arr[number] = {}
        try:
            with open(fullfilename, encoding='cp932') as csvfile:
                for row in csvfile:
                    row = row.strip()
                    if row != '' and not (row.startswith(';') or row.startswith('；')):
                        row = commentRegex.split(row,1)[0].split(',')
                        if group == "CHARA" and len(row) >= 2:
                            subkey = row.pop(0) # e.g. CSTR etc
                            if not subkey in subkeyToVarname:
                                print("No ", subkey, " in subkeyToVarname", file=sys.stderr)
                            else:
                                subkey = subkeyToVarname[subkey]
                            if subkey == "ID": # ID
                                if number != int(row[0]):
                                    print("ID inside file doesn't match filename!", fullfilename, file=sys.stderr)
                                continue
                            if not subkey in res:
                                print(subkey, file=sys.stderr)
                                res[subkey] = {}
                            subsubkey = row.pop(0)
                            if (subkey == "TALENT" or subkey == "CFLAG") and (len(row) == 0 or row[0] == ''):
                                row = [1] # Presumably this should be set to 1 if the user failed to add a third column for it

                            if subkey == "CALLNAME" or subkey == "CVSCALLNAME": # row[3] is empty
                                res[subkey][number] = subsubkey
                            elif len(row) == 0:
                                print("Found short row:", subkey, subsubkey, fullfilename, file=sys.stderr)
                            else:
                                if ((subkey + 'ID') in res) and subsubkey in res[subkey + 'ID']:
                                    subsubkey = res[subkey + 'ID'][subsubkey] # e.g. use CSTRID to convert a japanese string into its ID
                                if not number in res[subkey]:
                                    res[subkey][number] = {}
                                res[subkey][number][subsubkey] = row.pop(0)
                                try:
                                    res[subkey][number][subsubkey] = int(res[subkey][number][subsubkey])
                                except:
                                    pass
                        else:
                            print("ignoring: '" + group + "'", len(row), row, fullfilename, file=sys.stderr)
                        #arr[number].append(row)
        except Exception as e:
            print("Can't read", fullfilename, e, file=sys.stderr)


print( json.dumps(res, ensure_ascii = False, indent=1))
