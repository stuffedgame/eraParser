
/* lexical grammar */
%lex
/* We start in the INITIAL state.
   %x means exclusive - we only match rules with that name.
   %s means we also match the unnamed rules */
%x unquotedstring
%x unquotedstringAT
%x unquotedstringonly
%s expressionInString
%s conditionInAtString
%%
\n[ \t]*                                           return "NewLine"; /* Preserve indentation of next line */
[ \t]+                                             /* skip whitespace */
[;][^\r\n]*                                        return 'COMMENT';
SKIPSTART.*SKIPEND                                 return 'COMMENT'; /* skip  comments - BUT WARNING THIS IS GREEDY! */
[@][\"]([^%{"]|[%][^%]*[%]|[{][^}]*[}])*[\"]                          return 'INTERPRETED_STRING';
[@][^\" ,=\n]+                                     return 'DeclareFunction';
PRINT(L|W)?([ ]|\b)                                { this.begin("unquotedstringonly"); return 'PRINT_AND_TEXT'; }
<unquotedstringonly>[^\n]+                         return "UNQUOTED_STRING";
<unquotedstringonly>\n[ \t]*                       { this.popState(); return "NewLine" }

PRINT(SINGLE|PLAIN)?(V|S|FORMS?)?(C|LC)?(K|D)?(L|W)?([ ]|\b) { this.begin("unquotedstring"); return 'PRINT_AND_TEXT'; }
<unquotedstring>[^@\\%\n]+                         return "UNQUOTED_STRING";
<unquotedstring>[\\][@]                            { this.begin("conditionInAtString"); return "\\@";} /* like \@ e ? Hello # bye \@ */
<unquotedstring>[%]                                { this.begin("expressionInString"); return 'START_EXPRESSION_IN_STRING'; }
<unquotedstring>\n[ \t]*                           { this.popState(); return "NewLine" }
<expressionInString>[%]                            { this.popState(); return 'END_EXPRESSION_IN_STRING'; }

<conditionInAtString>[?]                           { this.begin("unquotedstringAT"); return '@?'; }
<unquotedstringAT>[^@\\%#\n]+                      return "UNQUOTED_STRING";
<unquotedstringAT>[%]                              { this.begin("expressionInString"); return 'START_EXPRESSION_IN_STRING'; }
<unquotedstringAT>[#]                              { return "@#"; }
<unquotedstringAT>[\\][@]                          { this.popState(); this.popState() /* conditionInAtString */; return '\\@_end';}

REUSELASTLINE[ ]?\b                                { this.begin("unquotedstring"); return 'PRINT_AND_TEXT'; }
(SELECTCASE|CASE|CASEELSE|BREAK|ENDSELECT)\b                return yytext;
(DO|LOOP|WHILE|WEND|CONTINUE)\b                    return yytext;
(REPEAT|REND)\b                                    return yytext;
["]([^\n"]|[\\]["])*["]                            return 'QUOTED_STRING';
(CC?ALLFORM|TRYCC?ALLFORM)\b                       { this.begin("unquotedstring"); return yytext; }
(CALL|TRYCALL|TRYCCALL|CATCH|ENDCATCH|CALLF)\b           return yytext;
(IF|ELSE|ELSEIF|ENDIF|SIF)\b                       return yytext;
(CLEARBIT|SETBIT|GETBIT|INVERTBIT|CVARSET)\b       return yytext;
(FOR|NEXT|LOCALS?|BEGIN|SAVEGLOBAL|LOADGLOBAL|LOADDATA|SAVEDATA|CLEARLINE|RESET_STAIN)\b    return yytext;
(DIMS?|FUNCTIONS?|DEFINE|RANDOMIZE|LOCALSIZE|LOCALS?SIZE|DYNAMIC|SPLIT|REDRAW|VARSET|PRI)    return yytext;
(QUIT|RESTART|RETURNF?)\b                          return yytext;
(DRAWLINE|TIMES|SWAP|RAND|WAIT|DELDATA|CHKDATA|GETMILLISECOND|GETTIME|SETCOLOR|RESETCOLOR|INPUTS?)\b                    return yytext;
"GOTO"[ ][^\n]+                                    return 'GOTO_LABEL';
"CUSTOMDRAWLINE"[ ][^\n]+                          return 'CUSTOMDRAWLINE';
(ADDCHARA|DELCHARA)                                return yytext; /* add/remove custom characters */
("=="|"<="|">="|"!="|">"|"<")                      return 'COMPARISON';
[\\][@]                                            return '\\@';
[?][^#]+[#][^\n]*[\\][@]                           return 'ENDAT';
[0-9]+([.][0-9]+)?                                 return 'NUMBER';
"&&"                                               return '&&';
"||"                                               return '||';
"'="                                               return "'=";
[=][^\n]*                                          return 'EQUAL_TEXT';
"++"                                               return yytext;
"--"                                               return yytext;
("+="|"-="|"*="|"/="|"%=")                         return 'SET_VAL';
"$"[^\n \t]*                                       return 'LABEL_FOR_GOTO';
"ARG"[:][0-9]+\b                                   return 'ARG';
"ARGS"[:][0-9]+\b                                  return 'ARGS';
"ARGS"\b                                           return 'ARGS';
"ARG"\b                                            return 'ARG';
"[[".*"]]"                                         return 'DOUBLE_BRACKET'; //FIXME
"["                                                return yytext;
"]"                                                return yytext;
[\*\/\-\+!^()\{\},\?#%:&|]                           return yytext;
[^\]0-9= \*\+\-\%\<\>\s\n\(\)\{\}\:\\,@%][^\]\\\,{\}= \*\+\-\%\<\>\:\(\)\s\n@%]*   return 'VAR';
<<EOF>>                                            return 'EOF';

/lex

/* operator associations and precedence */

%left 'SET_VAL'
%left '&&' '||'
%left 'COMPARISON'
%left '-'
%left '+'
%left '*'
%left '/'
$left '!'
%left '^'
%left '++'
%left ':'
%left 'ARG' 'ARGS' 'VAR'
%left '(' ')'

%% /* language grammar */

topLevel
    : lines
        { return $1 + (this.insideFunction ? '};' : '') /*return 'async function __MYEVALSTART() {\n' + $1 + (this.insideFunction ? '}; await FUNCS["EVENTFIRST"](); ' : '') + '\n}; __MYEVALSTART();'*/ }
    ;

lines
    : line /* Or use noisyLine for debugging */
        {$$ = $1;}
    | lines line /* Or use noisyLine for debugging */
        {$$ = $1 + $2;}
    ;

noisyLine
    : line
        {console.log($1); $$ = $1}
    ;

line
    : endOfLine
        {$$ = $1 }
    | lineWithoutNewline endOfLine
        {$$ = $1 + ';' + $2}
    ;

VarWithAssignment
    : Var EQUAL_TEXT
        {$$ = $1 + ' ' + escape_quotes($2)}
    | Var
        {$$ = $1}
    ;

Comma_Seperated_Arguments_With_Defaults
    : Comma_Seperated_Arguments_With_Defaults ',' + VarWithAssignment
        {$$ = $1 + ',' + $3}
    | VarWithAssignment
        {$$ = $1}
    ;

lineWithoutNewline
    : DeclareFunction ',' Comma_Seperated_Arguments_With_Defaults
        {$$ = (this.insideFunction ? '}; ' : '') + 'FUNCS["' + $1.substr(1) + '"] = async function('+ $3 + ") {"; this.insideFunction = true; }
    | DeclareFunction
        {$$ = (this.insideFunction ? '}; ' : '') + 'FUNCS["' + $1.substr(1) + '"] = async function() {'; this.insideFunction = true; }
    | lineWithoutNewlineInsideFunction
    ;

lineWithoutNewlineInsideFunction
    : forLoop
    | BEGIN Var
        {$$ = 'begin("' + $2 + '"); throw "Finished"'}
    | ifExpr
    | LOADGLOBAL
        {$$ = 'loadGlobal()'}
    | SAVEGLOBAL
        {$$ = 'saveGlobal()'}
    | LOADDATA Var
        {$$ = 'loadData(' + $2 + ')'}
    | SAVEDATA e ',' e
        {$$ = 'saveData(' + $2 + ',' + $4 + ')'}
    | Call
        {$$ = $1}
    | '#' Preprocessor
        {$$ = $2}
    | REPEAT e endOfLine lines REND
        {$$ = 'for(let COUNT = 0; COUNT < (' + $2 + '); COUNT++) {' + $3 + $4 + '}'}
    | LValueObject EQUAL_TEXT
        {$$ = '' + $1 + ' = "' + escape_quotes($2.substr(1).trim()) + '"'}
    | LValueObject SET_VAL e
        {$$ = '' + $1 + ' ' + $2 + $3}
    | LValueObject '++'
        {$$ = $1 + $2}
    | LValueObject '--'
        {$$ = $1 + $2}
    | ARG '++'
        {$$ = $1 + $2}
    | ARG '--'
        {$$ = $1 + $2}
    | DO endOfLine lines LOOP e
        {$$ = 'do {' + $2 + $3 + '} while(' + $5 + ')';}
    | WHILE e endOfLine lines WEND
        {$$ = 'while(e) { ' + $3 + $4 + ' }'}
    | PRINT_AND_TEXT
        {
            $$ = 'await printLine("' + $1.trimRight() + '")';
        }
    | PRINT_AND_TEXT String
        {
            $$ = 'await printLine("' + $1.trimRight() + '",' + $2 + ')';
        }
    | WAIT
        {$$ = 'await wait()'}
    | CLEARLINE e
        {$$ = 'clearLine(' + $2 + ')'}
    | SELECTCASE e endOfLine lines ENDSELECT
        {$$ = '{ let SELECTCASE_CONDITION = ' + $2 + '; if(false) {' + $3 + $4 + '} }';}
    | CASE COMMA_SEPERATED_LIST
        {$$ = '} else if ( ' + $2.map(x => '(SELECTCASE_CONDITION == ' + x + ')').join(' || ') + ') { '; }
    | CASEELSE
        {$$ = '} else {'}
    | BREAK
        {$$ = 'break'}
    | CONTINUE
        {$$ = 'continue'}
    | CLEARBIT Var ',' e
        {$$ = $2 + ' &= ~(1 << (' + $4 + '))'}
    | SETBIT Var ',' e
        {$$ = $2 + ' |= 1 << (' + $4 + ')'}
    | INVERTBIT Var ',' e
        {$$ = $2 + ' ^= 1 << (' + $4 + ')'}
    | SWAP LValueObject ',' LValueObject
        {$$ = 'swap(' + $2 + ',' + $4 + ')'}
    | CVARSET Var ',' e ',' e
        {$$ = 'cvarset(' + $2 + ',' + $4 + ',' + $6 + ')'}
    | RESET_STAIN e
        {$$ = 'resetStain(' + $2 + ')'}
    | SPLIT Var ',' Var ',' Var
        {$$ = 'split(' + $2 + ',' + $4 + ',' + $6 + ')' }
    | REDRAW e
        {$$ = 'reraw(' + $2 + ')';}
    | CUSTOMDRAWLINE
        {$$ = 'customDrawLine("' + $1.substr(15) + '")'}
    | DRAWLINE
        {$$ = 'drawLine()'}
    | TIMES Var ',' e
        {$$ = 'times(' + $2 + ',' + $4 + ')' }
    | VARSET COMMA_SEPERATED_LIST
        {$$ = 'varSet(' + $2.join(',') + ')' }
    | RESTART
        {$$ = 'restart()'}
    | RETURN e /* We need to make a note that this returns an int, not a string */
        {$$ = 'return ' + $2 }
    | RETURN
        {$$ = 'return'}
    | RETURNF e
        {$$ = 'return ' + $2 }
    | RETURNF
        {$$ = 'return'}
    | DELDATA Var
        {$$ = 'delData(' + $2 + ')'}
    | GETMILLISECOND
        {$$ = 'RESULT = getMilliseconds()'}
    | GETTIME
        {$$ = 'RESULT = getTime()'}
    | SETCOLOR e
        {$$ = 'setColor(' + $2 + ')' }
    | RESETCOLOR
        {$$ = 'resetColor()' }
    | RANDOMIZE e
        {$$ = 'randomize(' + $2 + ')'}
    | CHKDATA Var
        {$$ = 'chkData(' + $2 + ')'}
    | INPUT
        {$$ = 'input()'}
    | INPUTS
        {$$ = 'input()'}
    | GOTO_LABEL /* Marked with a LABEL_FOR_GOTO */
        {$$ = '/*goto ' + $1.substr(5) +'*/' }
    | LABEL_FOR_GOTO
        {$$ = '/*label ' + $1.substr(1) +'*/' }
    | QUIT
        {$$ = 'exit(0)'}
    | ADDCHARA e /* Add a character */
        {$$ = 'addChara(' + $2 + ')' }
    | DELCHARA e
        {$$ = 'delChara(' + $2 + ')' }
    | ENCODETOUNI Var
        {$$ = 'encodeToUni(' + $2 + ')' }
    ;

endOfLine
    : NewLine
        {$$ = $1;}
    | COMMENT NewLine
        {$$ = "// " + $1.substr(1) + $2;}
    | EOF
        {$$ = $1;}
    ;

forLoop
    : FOR Var ',' e ',' e endOfLine lines NEXT
        { $$ = 'for(' + $2 + ' = ' + $4 + '; ' + $2 + ' < ' + $6 + '; ' + $2 + '++) {' + $7 + $8 + '}' }
    ;

emptyLines
    : emptyLines endOfLine
        {$$ = $1 + $2}
    | endOfLine
        {$$ = $1}
    ;

ifExpr
    : IF e endOfLine lines ENDIF
        {$$ = 'if (' + $2 + ') {' + $3 + $4 + '}'}
    | SIF e endOfLine lineWithoutNewline
        {$$ = 'if (' + $2 + ')' + $3 + '{ ' + $4 + '}'}
    | SIF e endOfLine emptyLines lineWithoutNewline
        {$$ = 'if (' + $2 + ') {' + $3 + $4 + $5 + '}'}
    | ELSEIF e
        {$$ = '} else if (' + $2 + ') {'}
    | ELSE
        {$$ = '} else {' }
    ;

e
    : e '+' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '-' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '*' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '/' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '%' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | '!' e
        {$$ = '(!' + $2 + ') ';}
    | e '&&' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '&' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '|' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '||' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ') ';}
    | e '?' e '#' e
        {$$ = '( ' + $1 + ') ' + $2 + '( ' + $3 + ')' + ':' + '( ' + $5 + ')';}
    | e '^' e
        {$$ = "Math.pow((" + $1 + "), (" + $3 + "))";}
    | '++' e
        {$$ = $1 + $2}
    | '--' e
        {$$ = $1 + $2}
    | '-' e %prec UMINUS
        {$$ = -$2;}
    | '(' e ')'
        {$$ = $1 + $2 + $3;}
    | e COMPARISON e
        {$$ = '(' + $1 + ')' + $2 + '(' + $3 + ')';}
    | Number
        {$$ = $1}
    | Var
        {$$ = $1;}
    | '%' e ',' e '%'
        {$$ = 'padStringWithSpaces($2, $4, true)'}
    | '%' e ',' e, RIGHT '%'
        {$$ = 'padStringWithSpaces($2, $4, true)'}
    | '%' e ',' e, LEFT '%'
        {$$ = 'padStringWithSpaces($2, $4, false)'}
    | '%' e '%'
        {$$ = $2;}
    | '{' e ',' e '}'
        {$$ = 'padStringWithSpaces($2.toString(), $4, true)'}
    | '{' e ',' e, RIGHT '}'
        {$$ = 'padStringWithSpaces($2.toString(), $4, true)'}
    | '{' e ',' e, LEFT '}'
        {$$ = 'padStringWithSpaces($2.toString(), $4, false)'}
    | '{' e '}'
        {$$ = $2.toString();}
    | GETBIT '(' Var ',' e ')'
        {$$ = $3 + ' &= 1 << (' + $5 + ')'}
    | GETTIME '(' ')'
        {$$ = 'getMilliseconds()'}
    ;

Number
    : NUMBER
        {$$ = Number($1);}
    ;

LValueBase
    : VAR
    | LOCAL
    | LOCALS
    | ARGS
    ;

LValueObject
    : LValueBase
        {const base = $1; $$ = { base, params: [], dimensionSize: D[$1],
            toString() {
                if (typeof this.dimensionSize === 'number')
                    this.params.length = this.dimensionSize;
                /* The language has a strange quirk that it auto adds [0] for each missing dimension */
                return this.base + Array.from(this.params).map(x => '[' + (x ? x : '0') + ']').join('');
            }
        } }
    | LValueObject ':' Var
        {$$ = $1; $$.params.push($1.base + '.nameToIndex[' + $3 + ']') }
    | LValueObject ':' Number
        {$$ = $1; $$.params.push($3) }
    | LValueObject ':' '(' e ')'
        {$$ = $1; $$.params.push($1.base + '.nameToIndex[' + $4 + ']') }
    | LValueObject ':' VAR
        {$$ = $1; $$.params.push(nameToIndex($3, $1.base)) }
    ;

Var
    : Var '{' e '}' /* Seems ambigious with e | '{' e '}' */
        { $$ = $1 + '[' + $3 + ']' }
    | LValueObject
        { $$ = '' + $1 }
    | '\\@' e ENDAT
        { $$ = '"' + escape_quotes($2.substr(1, -1)) + '"' }
    | QUOTED_STRING
        { $$ = $1 }
    | INTERPRETED_STRING
        { $$ = '"' + escape_quotes($1.substr(1)) + '"' }
    | DOUBLE_BRACKET
        { $$ = '"' + escape_quotes($1) + '"' }
    | ARG
        { { const n = $1.substr(4); $$ = 'ARG' + (n ? n : '0') } }
    | ARGS
        { { const n = $1.substr(5); $$ = 'ARGS' + (n ? n : '0') } }
    | RAND '(' e ',' e ')'
        {$$ = 'randomIntFromInterval(' + $3 + ',' + $5 + ')'}
    | RAND '(' e ')'
        {$$ = 'randomIntFromInterval(0,' + $3 + ')'}
    | RAND ':' e
        {$$ = 'randomIntFromInterval(0,' + $3 + ')'}
    | VAR '(' ')'
        {$$ = $1 + '()';}
    | VAR '(' COMMA_SEPERATED_LIST ')'
        {$$ = $1 + '(' + $3.join(',') + ')';}
    ;

String
    : StringPart
        { $$ = $1 }
    | String StringPart
        { $$ = $1 + '+' + $2 }
    ;
StringPart
    : UNQUOTED_STRING
        { $$ = '"' + $1 + '"' }
    | START_EXPRESSION_IN_STRING e END_EXPRESSION_IN_STRING
        { $$ = '"" + ' + $2 }
    | START_EXPRESSION_IN_STRING e ',' e END_EXPRESSION_IN_STRING
        { $$ = '"" + ' + $2 /* TODO: Pad by number.  right? */}
    | START_EXPRESSION_IN_STRING e ',' e ',' VAR END_EXPRESSION_IN_STRING
        { $$ = '"" + ' + $2 /* TODO: Pad by number.  left, right or center based on 'VAR' */}
    | '\@' e '@?' String '@#' String '\@_end'
        { $$ = '((' + $2 + ') ? ' + $4 + ' : ' + $6 + ' )' }
    ;

COMMA_SEPERATED_LIST
    : e ',' COMMA_SEPERATED_LIST
        {$$ = [$1, ...$3] }
    | e
        {$$ = [$1]}
    | ',' COMMA_SEPERATED_LIST /* In a function call, this means use the default value */
        { $$ = ['undefined', ...$2]}
    ;

CallNameAndArgs
    : VAR '(' COMMA_SEPERATED_LIST ')'
        {$$ = 'await (await getFunc("' + $1 + '"))(' + $3.join(',') + ')'}
    | VAR ',' COMMA_SEPERATED_LIST
        {$$ = 'await (await getFunc("' + $1 + '"))(' + $3.join(',') + ')'}
    | VAR
        {$$ = 'await (await getFunc("' + $1 + '"))()'}
    ;
CallFormNameAndArgs
    : String '(' COMMA_SEPERATED_LIST ')'
        {$$ = 'await (await getFunc(' + $1 + '))(' + $3.join(',') + ')'}
    | String ',' COMMA_SEPERATED_LIST
        {$$ = 'await (await getFunc(' + $1 + '))(' + $3.join(',') + ')'}
    | String
        {$$ = 'await (await getFunc(' + $1 + '))()'}
    ;
Call
    : CALL CallNameAndArgs /* TODO - set the result variable */
        {$$ = "RESULT = " + $2}
    | CALLF CallNameAndArgs /* Call and do not set the RESULT variable */
        {$$ = $2}
    | CALLFORM CallFormNameAndArgs
        {$$ = "RESULT = " + $2}
    | TRYCALL CallNameAndArgs
        {$$ = 'try { RESULT = ' + $2 + ' } catch(e) {}'}
    | TRYCALLFORM CallFormNameAndArgs
        {$$ = 'try { RESULT = ' + $2 + ' } catch(e) {}'}
    | TRYCCALL CallNameAndArgs endOfLine CATCH lines ENDCATCH
        {$$ = 'try { RESULT = ' + $2 + ' } catch(e) {' + $3 + $5 + '}'}
    | TRYCCALLFORM CallFormNameAndArgs endOfLine CATCH lines ENDCATCH
        {$$ = 'try { RESULT = ' + $2 + ' } catch(e) {' + $3 + $5 + '}'}
    | TRYCCALL CallNameAndArgs endOfLine lines CATCH lines ENDCATCH
        {$$ = 'try { RESULT = ' + $2 + ';' + $3 + $4 + ' } catch(e) {' + $6 + '}'}
    | TRYCCALLFORM CallFormNameAndArgs endOfLine lines CATCH lines ENDCATCH
        {$$ = 'try { RESULT = ' + $2 + ';' + $3 + $4 + ' } catch(e) {' + $6 + '}'}
    ;


DimArgsObject
    : Var ',' COMMA_SEPERATED_LIST '=' COMMA_SEPERATED_LIST
        {$$ = {var: $1, numDimensions: 1, initialValue: ' = [' + $5.join(',') + ']'}}
    | Var '=' COMMA_SEPERATED_LIST
        {$$ = {var: $1, numDimensions: 1, initialValue: ' = [' + $3.join(',') + ']'}}
    | Var ',' COMMA_SEPERATED_LIST
        {$$ = {var: $1, numDimensions: $3.length, initialValue: ' = ' + $3.reduce(x => '[' + x + ']', '')}}
    | Var
        {$$ = {var: $1, numDimensions: 0, initialValue: ''} }
    ;

Preprocessor
    : DIM DimArgsObject
        {$$ = 'var ' + $2.var + $2.initialValue; D[$2.var] = $2}
    | DIM DYNAMIC DimArgsObject
        {$$ = 'var ' + $3.var + $3.initialValue; D[$3.var] = $3}
    | DIMS DimArgsObject
        {$$ = 'var ' + $2.var + $2.initialValue; DS[$2.var] = $2}
    | DIMS DYNAMIC DimArgsObject
        {$$ = 'var ' + $3.var + $3.initialValue; DS[$3.var] = $3}
    | DIM CONST DimArgsObject
        {$$ = 'const ' + $3.var + $3.initialValue; D[$3.var] = $3}
    | DIMS CONST DimArgsObject
        {$$ = 'const ' + $3.var + $3.initialValue; DS[$3.var] = $3}
    | PRI
        {$$ = $1}
    | FUNCTION
        {$$ = '/* ' + $1 + ' */'}
    | FUNCTIONS
        {$$ = '/* ' + $1 + ' */'}
    | DEFINE Var e
        {$$ = 'const ' + $2 + '=' + $3}
    | LOCALSIZE e
        {$$ = '/* ' + $1 + ' '  + $2 + ' */'}
    | LOCALSSIZE e
        {$$ = '/* ' + $1 + ' '  + $2 +' */'}
    ;

%%

// keep track of the dimension of variables
// We add to this with the preprocessor command '#DIM' etc
// NOTE: What about functions?  We should probably add these too.
const D = { FLAG: 1, DAY: 1, MONEY: 1, MASTER: 0 };
const DS = {}; // String variant

function escape_quotes(str) {
    return str.replace(new RegExp('["]', 'g'), '\\"');
}

function nameToIndex(x, base) {
    if (typeof D[x] !== 'undefined')
        return x;
    if (csvData[base + 'ID'] && csvData[base + 'ID'][x] )
        return csvData[base + 'ID'][x];
    else
        return base + '.nameToIndex["' + x + '"]';
}
