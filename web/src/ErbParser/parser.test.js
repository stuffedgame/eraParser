import React from 'react';
import ReactDOM from 'react-dom';

import { erbParser } from './index';

it('erbparser works', (done) => {
    loadParser().then(parser => {
        eval(parser.parse('1+1'));
        done();
    });
});
