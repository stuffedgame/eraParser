import React from 'react';
import { Parser } from "jison";
import { decode } from "iconv-lite";

/* eslint import/no-webpack-loader-syntax: off */
import jison from '!raw-loader!./grammer.jison';
import csvData from '../csv.json';
import erbToJsonIndex from '../erbToJsonIndex.json';

//import jison from "./grammer.jison";

function loadParser() {
    //const response = await fetch(jison);
    //const response = jison;
    //const text = await response.text();
    const text = jison;
    window.csvData = csvData;
    return new Parser(text/*, {debug: true}*/);
}

const erbParser = loadParser();

async function fetchFile_JIS(fileName) {
    const response = await fetch(fileName);
    const arrayBuffer = await response.arrayBuffer();
    const rawText = decode(new Buffer(arrayBuffer), 'shift_jis').toString();
    return rawText;
}

async function fetchErb(fileName) {
    let rawText = await fetchFile_JIS('ERB/' + fileName);
    let erb_js = erbParser.parse(rawText);
    rawText = '\n' + rawText;
    erb_js = 'let __RUNNER = async function() {\n' + erb_js + ' \n}; __RUNNER();\n//# sourceURL=' + fileName;
    return {rawText, erb_js};
};

async function fetchCsv(fileName) {
    let rawCsv = await fetchFile_JIS(fileName);
    let csv = rawCsv.split('\n').filter(x => !x.startsWith(';')).map(x => { x = x.split(','); x[0] = parseInt(x[0], 10); return x } );
    const obj = [];
    const objNames = [];
    obj.nameToIndex = {};
    csv.forEach(x => {
        objNames[x[0]] = x[1];
        obj.nameToIndex[x[1]] = x[0];
    });
    return [obj, objNames];
};

function initVar(variable, varName) {
    const sizeOfVar = csvData['VARIABLESIZE'][varName];
    if (typeof variable === 'undefined')
        variable = {};
    variable.nameToIndex = csvData[varName + 'ID'];
    if (sizeOfVar) {
        for(let i = 0; i < sizeOfVar; ++i) {
            if(typeof variable[i] === 'undefined') {
                if(varName === 'FLAG')
                    variable[i] = 0
                else
                    variable[i] = {}
            }
        }
    }
    return variable;
}

async function runErb(cb_setState, cb_setOnEnterPressListener, cb_setOnClickOrKeyPressListener, cb_linkClicked) {
    try {
        var {rawText, erb_js} = await fetchErb("SYSTEM.ERB");
        //var {rawText, erb_js} = await fetchErb("TEST.ERB");
    } catch(e) {
        console.error('Failed to compile erb', e);
    }
    cb_setState({source: rawText, sourceJS: erb_js});
    var displayText = []; /* Clickable */
    var historyText = []; /* Above displayText, but not clickable */
    var lineNumber = 0;
    /* Setup functions to be used by the eval */

    /* eslint no-unused-vars: off */
    async function wait(waitForEnterKey = false) {
        if (waitForEnterKey) {
            return await new Promise(resolve =>
                cb_setOnEnterPressListener(value => {
                    cb_setOnEnterPressListener(null);
                    resolve(value);
                    return true; /* Clear input box */
                })
            );
        } else {
            return await new Promise(resolve =>
                cb_setOnClickOrKeyPressListener(key => {
                    cb_setOnClickOrKeyPressListener(null);
                    resolve(key);
                    return true; /* Clear input box */
                })
            );
        }
    }

    function print(x) {
        ++lineNumber;
        const shortcutKey = x && x.match && x.match(/^\[([0-9]+)\]/);
        let newLine;
        if (shortcutKey && shortcutKey[1])
            newLine = <div className="lineOfText clickable" key={lineNumber} onClick={() => cb_linkClicked(shortcutKey[1])}>{x}</div>
        else
            newLine = <div className="lineOfText" key={lineNumber}>{x}</div>;
        displayText = [ ...displayText, newLine];
        cb_setState({displayText});
    }

    /* Called when we no longer want the links etc to be clickable */
    function moveDisplayTextToHistory() {
        historyText = [...historyText, ...displayText];
        displayText = [];
        cb_setState({displayText, historyText});
    }

    /* eslint no-unused-vars: off */
    async function printLine(cmd, x) {
        print(x);
        if (cmd.includes('W'))
            await wait();
    };
    /* eslint no-unused-vars: off */
    const clearLine = (numLines) => {
        const numToRemoveFromHistory = displayText.length - numLines;
        if (numToRemoveFromHistory > 0)
            historyText = historyText.slice(0, -numToRemoveFromHistory)
        displayText = displayText.slice(0, -numLines);
        cb_setState({displayText, historyText});
    }

    function randomIntFromInterval(min, max) {
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    function SUBSTRING(str,start,end) {
        return str.substring(start, end);
    }
    function STRLENS(str) {
        return str.length;
    }
    function STRCOUNT(str, innerstr) {
        return str.split(innerstr).length - 1;
    }

    var FUNCS = {
        "INPUTINT": async function(/* arbitrary number */) {
            var options = Array.from(arguments);
            const waitForEnterKey = !! options.find(x => x.toString().length > 1);
            do {
                RESULT = await wait(waitForEnterKey);
                if (typeof(+RESULT) === 'number' && !isNaN(+RESULT)) {
                    RESULT = +RESULT // Convert to a number if we can
                }
            } while (options.indexOf(RESULT) === -1);
            print(RESULT);
            moveDisplayTextToHistory();
            return RESULT;
        }
    };

    /* eslint no-unused-vars: off */
    const DAY = [];
    const MONEY = [];
    const LOCAL = [];
    const LOCALS = [];

    var {TRAIN, TRAINNAME, BASE, BASENAME,
           EQUIP, EQUIPNAME,TEQUIP, TEQUIPNAME,
           STAIN, STAINNAME, EX, EXNAME, SOURCE, SOURCENAME,
           FLAG, FLAGNAME, TFLAG, TFLAGNAME, CFLAG, CFLAGNAME,
           TCVAR, TCVARNAME, STR, STRNAME,TSTR, TSTRNAME,
           CSTR, CSTRNAME, SAVESTR, SAVESTRNAME, CDFLAG,
           CDFLAGNAME, GLOBAL, GLOBALNAME, GLOBALS, GLOBALSNAME,
           ITEM, ITEMNAME, TALENT, TALENTNAME} = csvData;

    TRAIN = initVar(TRAIN, 'TRAIN');
    BASE = initVar(BASE, 'BASE');
    EQUIP = initVar(EQUIP, 'EQUIP');
    EQUIP = initVar(TEQUIP, 'TEQUIP');
    STAIN = initVar(STAIN, 'STAIN');
    EX = initVar(EX, 'EX');
    SOURCE = initVar(SOURCE, 'SOURCE');
    FLAG = initVar(FLAG, 'FLAG');
    TFLAG = initVar(TFLAG, 'TFLAG');
    CFLAG = initVar(CFLAG, 'CFLAG');
    TCVAR = initVar(TCVAR, 'TCVAR');
    STR = initVar(STR, 'STR');
    STR = initVar(TSTR, 'TSTR');
    CSTR = initVar(CSTR, 'CSTR');
    SAVESTR = initVar(SAVESTR, 'SAVESTR');
    CDFLAG = initVar(CDFLAG, 'CDFLAG');
    GLOBAL = initVar(GLOBAL, 'GLOBAL');
    GLOBALS = initVar(GLOBALS, 'GLOBALS');
    ITEM = initVar(ITEM, 'ITEM');
    TALENT = initVar(TALENT, 'TALENT');


    var MONEYLABEL = '$';
    var DRAWLINESTR = "--------------------------------- -------------------------------------------------------------------- ------------------------- ";
    var WINDOW_TITLE = 'EMU WEB';
    var TARGET = Array(csvData['VARIABLESIZE']['TARGET']);
    var ASSI = Array(csvData['VARIABLESIZE']['ASSI']);
    var MASTER = 0;
    var RESULT = 0;

    function loadGlobal() { /* TODO */}
    function call(str) { /* TODO */ }
    function begin(str) { /* TODO */ }
    let getFunc = null;

    async function evalErbJs(erb_js) {
        try {
            /* eslint no-eval: off */
            const evalRet = await eval(erb_js);
        } catch(e) {
            const erbAsArray = erb_js.split('\n');
            let errorMsg = "";
            if(e.lineNumber >= 2) {
                errorMsg = erbAsArray[e.lineNumber-2] + '\n' +
                        erbAsArray[e.lineNumber-1] + '\n' +
                        Array(e.columnNumber+1).join(" ") + '^\n' +
                        'Line ' + (e.lineNumber-1) + ':';
            }
            console.warn(errorMsg);
            console.warn(e);
            errorMsg += e.toString();
            cb_setState({errorMsg, errorLineNumber: e.lineNumber-1});
        }
    }
    getFunc = async function(funcName) {
        var func = FUNCS[funcName];
        if(func)
            return func;
        const s = funcName.lastIndexOf('_');
        var fileIndex = '';
        if (s === -1)
            fileIndex = erbToJsonIndex['functions'][funcName][''];
        else
            fileIndex = erbToJsonIndex['functions'][funcName.substring(0, s)][funcName.substring(s+1)];
        if (typeof fileIndex === 'undefined') {
            console.error("Can't find file with function", funcName);
            return null;
        }
        const filename = erbToJsonIndex['filename'][fileIndex]
        if (typeof fileIndex === 'undefined') {
            console.error("Can't find file with function", funcName, "index", fileIndex);
            return null;
        }
        console.log("Loading...", filename);
        var {rawText, erb_js} = await fetchErb(filename);
        await evalErbJs(erb_js);
        func = FUNCS[funcName];
        if(func) {
            console.log("Successfully loaded and found", funcName);
            return func;
        }
        console.error('Loaded ', filename, 'but still no', funcName, '!');
        return null;
    }

    await evalErbJs(erb_js);
    await (await getFunc("EVENTFIRST"))();
    print("(ENDED)")

}

export { erbParser, fetchErb, runErb };
