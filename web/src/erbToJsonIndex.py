#!/usr/bin/python3

from pathlib import Path
import re
import json
import csv
import sys

res = {}
filename = []
res['filename'] = filename
functions = {}
res['functions'] = functions


functionDefinitionRegex = re.compile(r'^@([^,\r\n\s(]*)')

for path in Path('../../ERB').glob('**/*.*'):
    fullfilename = str(path)

    try:
        with open(fullfilename, encoding='cp932', errors='ignore') as erbfile:
            filename.append(fullfilename[10:])
            f_index = len(filename) - 1

            for row in erbfile:
                matches = functionDefinitionRegex.match(row)
                if matches:
                    functionName = matches[1]
                    if len(functionName) > 0:
                        parta = functionName
                        partb = ''
                        try:
                            parta, partb = functionName.rsplit('_', 1)
                        except:
                            pass
                        if not parta in functions:
                            functions[parta] = {}
                        functions[parta][partb] = f_index

    except Exception as e:
        print("Can't read", fullfilename, e, file=sys.stderr)

print( json.dumps(res, ensure_ascii = False))
